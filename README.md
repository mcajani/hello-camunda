How to attach to running container
======

	docker exec \
	    --interactive \
	    --tty \
	    workspace_camunda_1 \
	    /bin/bash
